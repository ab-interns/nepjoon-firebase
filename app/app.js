
//------------------------------------- FireBase -----------------------------------------------//

var app = angular.module("sampleApp", ["firebase"]);
app.controller("SampleCtrl", function ($scope, $firebaseArray) {
    var ref = firebase.database().ref('Product');
    // download the data into a local object
    $scope.ref = $firebaseArray(ref);
    $scope.name = ref.child('name');
    $scope.price = ref.child('price');

//------------------------- Edit ------------------------------------//
var isEditing = false,
    tempNameValue = "",
    tempPriceValue = "";

$(document).on('click', '.edit', function() {
    var parentRow = $(this).closest('tr'),
        tableBody = parentRow.closest('tbody'),
        tdName = parentRow.children('td.name'),
        tdPrice = parentRow.children('td.price');

    if(isEditing) {
        var nameInput = tableBody.find('input[name="name"]'),
            priceInput = tableBody.find('input[name="price"]'),
            tdNameInput = nameInput.closest('td'),
            tdPriceInput = priceInput.closest('td'),
            currentEdit = tdNameInput.parent().find('td.edit');

        if($(this).is(currentEdit)) {
            // Save new values
            var tdNameValue = nameInput.prop('value'),
                tdPriceValue = priceInput.prop('value');

            tdNameInput.empty();
            tdPriceInput.empty();

        }
        //----------- Add to firebase ----------//
        var newObj = {'name':tdNameValue,'price': tdPriceValue};
        ref.push(newObj);

        currentEdit.html('<i class="fa fa-edit" style="font-size:20px"></i>');
        isEditing = false;
    }
    else {
        isEditing = true;
        $(this).html('<i class="fa fa-floppy-o" style="font-size:20px"</i>');

        var tdNameValue = tdName.html(),
            tdPriceValue = tdPrice.html();

    // Save current  values
         tempNameValue = tdNameValue;
         tempPriceValue = tdPriceValue;

         tdName.empty();
         tdPrice.empty();

       // Create input forms
        tdName.html('<input type="text" name="name" value="' + tdNameValue + '">');
        tdPrice.html('<input type="text" name="price" value="' + tdPriceValue + '">');

    }
});

// -------------------------------- Delete --------------------------------------//
$(document).on('click', '.trash', function() {

    // Turn off editing if row is current input
    if(isEditing) {
        var parentRow = $(this).closest('tr'),
            tableBody = parentRow.closest('tbody'),
            tdInput = tableBody.find('input').closest('td'),
            currentEdit = tdInput.parent().find('td.edit'),
            thisEdit = parentRow.find('td.edit');

        if(thisEdit.is(thisEdit)) {
            isEditing = false;
        }
    }

    // Remove table
    $(this).closest('tr');

    return firebase.database().ref().remove(); //Remove object in FireBase
});

//----------------------------------- Add ------------------------------------//

$('.new-row').on('click', function() {
    var tableBody = $(this).closest('tbody'),
        trNew = '<tr>' +
            '<td class="name"><input type="text" name="name" value=""></td>' +
            '<td class="price"><input type="text" name="price" value=""></td>' +
            '<td class="edit "><i class="fa fa-edit" style="font-size:20px"></i></td>' +
            '<td class="trash"><i class="fa fa-trash" style="font-size:20px"></i></td></tr>';

    if(isEditing) {
        var nameInput = tableBody.find('input[name="name"]'),
            priceInput = tableBody.find('input[name="price"]'),
            tdNameInput = nameInput.closest('td'),
            tdPriceInput = priceInput.closest('td'),
            currentEdit = tdNameInput.parent().find('td.edit');

        // Get current values
        var newNameInput = nameInput.prop('value'),
            newPriceInput = priceInput.prop('value');

        // Clear previous values
        tdNameInput.empty();
        tdPriceInput.empty();

        tdNameInput.html(newNameInput);
        tdPriceInput.html(newPriceInput);

        // Display static row edit icon
        currentEdit.html('<i class="fa fa-edit" style="font-size:20px"></i>');
    }

    isEditing = true;
    tableBody.find('tr:last').before(trNew);

});

});
